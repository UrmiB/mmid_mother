package com.bv.mmid_mother.interfaces;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by Administrator on 7/4/2016.
 */
public interface BindFragment {
    /**
     * Call to this method replaces the currently shown fragment with a new one
     *
     * @param claz           the class of the new fragment
     * @param addToBackStack whether the old fragment should be added to the back stack
     * @param args           arguments to be set for the new fragment
     */
    public void replaceFragment(Class<? extends Fragment> claz, boolean addToBackStack,
                                Bundle args, String mFragID);
}
