package com.bv.mmid_mother.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidcommonlibrary.util.Utils;
import com.bv.mmid_mother.NewScanActivity;
import com.bv.mmid_mother.R;
import com.bv.mmid_mother.constants.Constants;
import com.bv.mmid_mother.interfaces.BindFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment.
 * Use the {@link FlashFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FlashFragment extends Fragment implements BindFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    @BindView(R.id.tv_title_flash)
    TextView mTv_Title;
    @BindView(R.id.tv_title_flash_botm)
    TextView mTv_TextBottom;

    int mType_dest;
    private FragmentManager mManager;
    private FragmentTransaction mTransaction;

    public FlashFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FlashFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FlashFragment newInstance(String param1, String param2) {
        FlashFragment fragment = new FlashFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for getActivity() fragment
        View view =  inflater.inflate(R.layout.fragment_flash, container, false);

        ButterKnife.bind(this, view);

        /* GettingFragment Manager */
        mManager = getFragmentManager();
        initView(view);

        return view;
    }
    public void initView(View view){

        if (getArguments()!=null){
            mType_dest = getArguments().getInt("destination");
        }
        switch (mType_dest){
            case R.id.btn_feed_new:
                mTv_Title.setText(getResources().getString(R.string.flash_title_feed));
                mTv_TextBottom.setVisibility(View.VISIBLE);
                ((NewScanActivity)getActivity()).setToolBar(getResources().getString(R.string.dash_feed));
                break;
            case R.id.btn_fridge_new:
                mTv_Title.setText(getResources().getString(R.string.flash_title_fridge));
                mTv_TextBottom.setVisibility(View.GONE);
                ((NewScanActivity)getActivity()).setToolBar(getResources().getString(R.string.new_fridge));
                break;
            case R.id.btn_freeze_new:
                mTv_Title.setText(getResources().getString(R.string.flash_title_freeze));
                mTv_TextBottom.setVisibility(View.GONE);
                ((NewScanActivity)getActivity()).setToolBar(getResources().getString(R.string.new_freeze));
                break;
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                replaceFragment(BottleDataFragment.class, false, null, Constants.FRAG_TAG_NEW_BOTTLE_DATA);
            }
        }, 5000);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }//I have to travel out of city with family

    @Override
    public void onDetach() {
        super.onDetach();

    }


    @Override
    public void replaceFragment(Class<? extends Fragment> claz, boolean addToBackStack, Bundle args, String mId) {
// If the required fragment is already shown - do nothing
        if (isFragmentShown(claz)) {
            return;
        }

        mTransaction = mManager.beginTransaction();


        Fragment newFragment;

        try {
            // Create new fragment
            newFragment = claz.newInstance();
            if (args != null) newFragment.setArguments(args);
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
            return;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return;
        }

        if (addToBackStack) {
            // Add getActivity() transaction to the back stack
            mTransaction.addToBackStack(null);
        }

        // Change to a new fragment
        mTransaction.replace(R.id.rl_main_new, newFragment, mId);
        mTransaction.commit();
    }
    /**
     * Check whether a fragment of a specific class is currently shown
     * @param claz class of fragment to test. Null considered as "test no fragment shown"
     * @return true if fragment of the same class (or a superclass) is currently shown
     */
    private boolean isFragmentShown(Class<? extends Fragment> claz) {
        Fragment currFragment = mManager.findFragmentById(R.id.rl_main_new);

        return (currFragment == null && claz == null) ||
                (currFragment != null && claz.isInstance(currFragment));
    }
}
