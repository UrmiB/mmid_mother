package com.bv.mmid_mother.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androidcommonlibrary.util.Utils;
import com.bv.mmid_mother.R;
import com.bv.mmid_mother.constants.Constants;
import com.bv.mmid_mother.interfaces.BindFragment;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain getActivity() fragment
 * Use the {@link NewScanFragment#newInstance} factory method to
 * create an instance of getActivity() fragment.
 */
public class NewScanFragment extends Fragment implements BindFragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @BindView(R.id.btn_cancel_common)
    Button mBtn_cancel;
    @BindView(R.id.btn_home_common)
    Button mBtn_home;

    private DecoratedBarcodeView barcodeView;
    private static final int REQUEST_CAMERA = 3;

    private FragmentManager mManager;
    private FragmentTransaction mTransaction;

    public NewScanFragment() {
        // Required empty public constructor
    }

    /**
     * Use getActivity() factory method to create a new instance of
     * getActivity() fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NewAcceptFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NewScanFragment newInstance(String param1, String param2) {
        NewScanFragment fragment = new NewScanFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for getActivity() fragment
        View view =  inflater.inflate(R.layout.fragment_new_scan, container, false);

        ButterKnife.bind(this, view);

        /* GettingFragment Manager */
        mManager = getFragmentManager();
        initView(view);

        return view;
    }

    public void initView(View view){
        barcodeView = (DecoratedBarcodeView) view.findViewById(R.id.barcode_scanner);

        if( Utils.checkNeedsPermission(getActivity(), Manifest.permission.CAMERA)) {
            requestCameraPermission();
        }
        else
        {
            initBarcode();
        }
       new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                replaceFragment(NewAcceptFragment.class, true, null, Constants.FRAG_TAG_NEW_ACCEPT);
            }
        }, 2000);
    }
    private void initBarcode()
    {
        barcodeView.decodeContinuous(callback);
        barcodeView.setStatusText("");
    }

    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
        } else {
            // Eh, prompt anyway
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initBarcode();
            } else {
                // Permission denied
                Toast.makeText(getActivity(), "Your action can not perform without access to camera.", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(final BarcodeResult result) {
            if (result.getText() != null) {
                Log.d("System out", "\n---------------QR----------------\n" + result.getText());
                Toast.makeText(getContext(), "QR Code: " + result.getText(), Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        replaceFragment(NewAcceptFragment.class, true, null, Constants.FRAG_TAG_NEW_ACCEPT);
                    }
                }, 2000);
            }
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        barcodeView.resume();
    }

    @Override
    public void onPause() {
        super.onPause();

        barcodeView.pause();
    }

    public void pause(View view) {
        barcodeView.pause();
    }

    public void resume(View view) {
        barcodeView.resume();
    }

    public void triggerScan(View view) {
        barcodeView.decodeSingle(callback);
    }

    @OnClick(R.id.btn_cancel_common)
    public void onClickCancel(){
        Utils.goBack(mManager, getActivity());
    }
    @OnClick(R.id.btn_home_common)
    void onClickHome(){
        Utils.goBack(mManager, getActivity());
    }


    @Override
    public void replaceFragment(Class<? extends Fragment> claz, boolean addToBackStack, Bundle args, String mId) {
// If the required fragment is already shown - do nothing
        if (isFragmentShown(claz)) {
            return;
        }

        mTransaction = mManager.beginTransaction();


        Fragment newFragment;

        try {
            // Create new fragment
            newFragment = claz.newInstance();
            if (args != null) newFragment.setArguments(args);
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
            return;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return;
        }

        if (addToBackStack) {
            // Add getActivity() transaction to the back stack
            mTransaction.addToBackStack(null);
        }

        // Change to a new fragment
        mTransaction.replace(R.id.rl_main_new, newFragment, mId);
        mTransaction.commit();
    }
    /**
     * Check whether a fragment of a specific class is currently shown
     * @param claz class of fragment to test. Null considered as "test no fragment shown"
     * @return true if fragment of the same class (or a superclass) is currently shown
     */
    private boolean isFragmentShown(Class<? extends Fragment> claz) {
        Fragment currFragment = mManager.findFragmentById(R.id.rl_main_new);

        return (currFragment == null && claz == null) ||
                (currFragment != null && claz.isInstance(currFragment));
    }

}
