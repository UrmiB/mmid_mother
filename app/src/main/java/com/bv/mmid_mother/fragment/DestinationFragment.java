package com.bv.mmid_mother.fragment;

import android.Manifest;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidcommonlibrary.util.Utils;
import com.bv.mmid_mother.NewScanActivity;
import com.bv.mmid_mother.R;
import com.bv.mmid_mother.constants.Constants;
import com.bv.mmid_mother.interfaces.BindFragment;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment.
 * Use the {@link DestinationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DestinationFragment extends Fragment implements BindFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @BindView(R.id.btn_cancel_common)
    Button mBtn_cancel;
    @BindView(R.id.btn_home_common)
    Button mBtn_home;
    @BindView(R.id.btn_feed_new)
    TextView mBtn_Feed;
    @BindView(R.id.btn_fridge_new)
    TextView mBtn_Fridge;
    @BindView(R.id.btn_freeze_new)
    TextView mBtn_Freeze;

    private FragmentManager mManager;
    private FragmentTransaction mTransaction;

    public DestinationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DestinationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DestinationFragment newInstance(String param1, String param2) {
        DestinationFragment fragment = new DestinationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for getActivity() fragment
        View view =  inflater.inflate(R.layout.fragment_destination, container, false);

        ButterKnife.bind(this, view);

        /* GettingFragment Manager */
        mManager = getFragmentManager();
        initView(view);

        return view;
    }

    public void initView(View view){
        ((NewScanActivity)getActivity()).setToolBar(getResources().getString(R.string.new_dest));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @OnClick(R.id.ll_feed)
    public void onClickFeed(){
        Bundle bundle = new Bundle();
        bundle.putInt("destination", R.id.btn_feed_new);
        replaceFragment(FlashFragment.class, false, bundle, Constants.FRAG_TAG_NEW_FLASH);
    }

    @OnClick(R.id.ll_fridge)
    public void onClickFridge(){
        Bundle bundle = new Bundle();
        bundle.putInt("destination", R.id.btn_fridge_new);
        replaceFragment(FlashFragment.class, false, bundle, Constants.FRAG_TAG_NEW_FLASH);
    }

    @OnClick(R.id.ll_freeze)
    public void onClickFreeze(){
        Bundle bundle = new Bundle();
        bundle.putInt("destination", R.id.btn_freeze_new);
        replaceFragment(FlashFragment.class, false, bundle, Constants.FRAG_TAG_NEW_FLASH);
    }

    @OnClick(R.id.btn_cancel_common)
    public void onClickCancel(View view){
        Utils.goBack(mManager, getActivity());
    }
    @OnClick(R.id.btn_home_common)
    void onClickHome(){
        getActivity().finish();
    }


    @Override
    public void replaceFragment(Class<? extends Fragment> claz, boolean addToBackStack, Bundle args, String mId) {
// If the required fragment is already shown - do nothing
        if (isFragmentShown(claz)) {
            return;
        }

        mTransaction = mManager.beginTransaction();


        Fragment newFragment;

        try {
            // Create new fragment
            newFragment = claz.newInstance();
            if (args != null) newFragment.setArguments(args);
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
            return;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return;
        }

        if (addToBackStack) {
            // Add getActivity() transaction to the back stack
            mTransaction.addToBackStack(null);
        }

        // Change to a new fragment
        mTransaction.replace(R.id.rl_main_new, newFragment, mId);
        mTransaction.commit();
    }
    /**
     * Check whether a fragment of a specific class is currently shown
     * @param claz class of fragment to test. Null considered as "test no fragment shown"
     * @return true if fragment of the same class (or a superclass) is currently shown
     */
    private boolean isFragmentShown(Class<? extends Fragment> claz) {
        Fragment currFragment = mManager.findFragmentById(R.id.rl_main_new);

        return (currFragment == null && claz == null) ||
                (currFragment != null && claz.isInstance(currFragment));
    }

}
