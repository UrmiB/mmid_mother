package com.bv.mmid_mother.constants;

import android.os.Environment;

public class Constants {

    /* Storage files directory */
    public static String APP_HOME = Environment.getDataDirectory()
            .getPath() + "/MMID";
    public static String DIR_LOG = APP_HOME + "/Log";
    public static String LOG_ZIP = APP_HOME + "/MMID.zip";
    public static String DIR_IMAGES = APP_HOME + "/data";

    /* Your unique preference name */
    public static String PREF_FILE = "PREF_MMID";


    /* Tag for bind fragment */
    public static String FRAG_TAG_DASHBOARD = "fragment_dashboard";
    public static String FRAG_TAG_NEW_ACCEPT = "fragment_new_accept";
    public static String FRAG_TAG_NEW_DESTINATION = "fragment_new_dest";
    public static String FRAG_TAG_NEW_FLASH = "fragment_new_flash";
    public static String FRAG_TAG_NEW_BOTTLE_DATA = "fragment_new_bottle_data";
}
